<?php
$server_start = microtime(true) * 10000;
header("HTTP/1.1 502 Under Maintenance");
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="robots" content="none"/>
    <meta name="contact" content="kissarat@gmail.com"/>
    <meta name="author" content="Taras Labiak"/>
    <meta name="document-state" content="Static"/>
    <meta name="viewport"
          content="width=device-width height=device-height initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <title>Under Maintenance</title>
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Russo+One&subset=cyrillic">
    <script>
        var server_start = <?= $server_start ?>;
        var request_method = '<?= $_SERVER['REQUEST_METHOD'] ?>';
        <?php
            if ('GET' != $_SERVER['REQUEST_METHOD']) {
                $data = file_get_contents('php://input');
                if (!empty($data)) {
                    echo "var request_data = '$data';";
                }
            }
        ?>
    </script>
    <script src="statistics.js"></script>
</head>
<body>
<div id="root" style="display: none">
    <div id="text" style="font-family: 'Russo One', 'Segoe UI', 'Ubuntu', serif; color: white; font-size: 6vmin;
    margin-top: 30vh; text-align: center">
        Сайт в разработке
    </div>
    <img style="position: fixed; left: 3vmin; bottom: 3vmin;"
         src="data:image/jpg;base64,/9j/4AAQSkZJRgABAQEAYwBjAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCADIASwDAREAAhEBAxEB/8QAGwABAAMBAQEBAAAAAAAAAAAAAAIDBAEFBgf/xAAaAQEBAQEBAQEAAAAAAAAAAAAAAQIDBAUG/9oADAMBAAIQAxAAAAH8/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOnAAAAAAAAAAAAAAAAAAbubRlTpj2hQ4AAAAAAAAAAAAAAAdPc819nyb8305twqsxdmXop1KqiAAAAAAAAAAAAADp9x8npTp8d9TnZF0aeVsjiZtsPVwAAAAAAAAAAAAA6fo3wunkep8l9LnCgO19N8/WXo8L15AAAAAAAAAAAAAHT9U/Odfm/ovlfocqduQOn3Xxu3he3HhezHAAAAAAAAAAAAdJEi6P0T4PX53358j0RrMpc+5l6P0j876fl/qcvnPbzjXAAAAAAAAACUXGg1FtbI0l2Gn5XSjvLONwerPNMe5q436z4/o+Q+rx8f6GK9SorIETgAAAAAOlxrjeehG+ti6CZIt4ufA6+B+k4+HzunldvO14iX1vH08H248z3Y09JAoMxUVECIAAABabo9GPTt9BNZpJF5adjhk53H1nlmJdJt5zDw15Js568b14o6yRMmRKikqKzgAB0vPQPTy9OtBpsnV1SiNRSeb6OdcoUmeTzK8k9343XJu4/o8vJ08vpBnOEiwHCogRABabj0o9UsLKvNBIlUUiePHxa2G89KPpyMVkvLr5rbyfTngBGoiOg4VlZAiAaDcejl6BXVNegaC0hSTwj5xaQC89Q+0jhWUlJ5FQFnagVRUQKilaorIgF5sN0bjWeZXnkQQPPOAAmaj3j6YrjKZTJqUllcKY6TquMxQULSUxAAmaDUajUai0uLyw8g8UxlZabo9uvTimsBkInbPQs6Clc5QlMUFK1LUVREAFhcaDQXmgsLS46TIECzKK5dSKZ6rJGit6SWqSkoqgzlMVrWVLVEQAASJl5cWFwJHARS80ZueqLBKppoNNcOS0FKVFRSVLWtZXHAAAACZI6SJHSSTOr06ROiyZpSw4QIFZBaytayuIHAAAAAAAdOnQhenQcJBOlgUROHDhA4cOAAAAAAAAAAAAAAHTgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//EACsQAAICAgIBAgYCAgMAAAAAAAABAgMEEhETBRQhECAiMDFAIzIkM1BgcP/aAAgBAQABBQL/AMox6VfP0i65YlsRwkv+D8Zi+quy4ejvjOFkHPaMqoO708ZHR9PXL9/wEDzsv5xTlE759Ty+Z9tatS95f0/d8FHjC8st8mX5+Tx9SdGdCMf3vEx18dbLuyLFw/kwoa4PkP8AZ+zwas65EKJSli5ddOBCm2rNyq+LcequweJHpsw7a3oyMOvHyY7za9/1OBVtkcdsWKz0kyOL7LFIY+sraLqW3ZHHddbzY0rodGuNZJwWyszrD8x05brNDQ4++ockKCNBHGIYpHHFRwOs1SFD3zpKOHZ5TFiX50bSjIyPVV1OeLdNZGL0U4/kcnLrgQ/0J63/AEscYjSGhxNTj7UY8kKiFXvCojURUU04oU4H08pHDOC3GrugvHYsS7xNXP8AnYxX5OqR2UzjdS5YmXPujHbquX8v1HLQpo2QzgaOPsRiQgQgQikbDS554H+fcf0qX0kLZbC9xoaiiTpLo41zs8Wjx0LoU+QnCM94WGTjTk+LYEpzHM3Ow3OTj54ogiCRXHkteonwRk+Ezk9z8qXsZ96hj7MU2nDMyYlXlJ1lN8L651ykRx1Elk9RnOzLaouidV56eZ6eR0M6TpOo1NRr5okCBBmVz115MZKM+TsNnw58KWVCuNvk4osslbL4xnJGLZ13bcm5/YdZKmDH8OGaHA0vhqxxaHz80SLIyIzNi3CjJunKrO/KiepyRyyZL5URMLGbnx8JXJErnxsfk1GfUJM9hsbGMY/kRFkZEZCmKQpCkKaOeS/ApsJ+PuiPGuR02CxrpEPH2spxKqzf2nbwpWuY5HJ/Zqvg1iew2NjkOQ2N/Bj+VMUhSFM3IzOwVgrDc3Hckd/srGSuSXedzUZTbWx7kY7EUonI5DkOQ5DY2cnI2c/YTFI2FM3O07zuZ2MbEbE7EzY25NuSK5I8J7+247BzHIchsbOTk5+3ybGxscmxsbm5vwdhubHImKXtudg5m5sbmxsNnJz+hycnJycnJscmxsbnYdh2G5ubGxscnP8A3r//xAAhEQACAgEEAwEBAAAAAAAAAAAAAQIRMRAgIUADQVBwgP/aAAgBAwEBPwH8qr4kVY41sor4ECey/gwwS3RRLvRxvjgl3k6QsktK1S4H3VkrT2VwY09j7yyPGll8nrTixyVCwPu29y5QxYH8PxI8gnQ/hqVEpX/Rf//EADERAAEEAAMFBgQHAAAAAAAAAAEAAgMRBBIxBRMhIkAQICMyQVBRYXBxMDNCYIGRsf/aAAgBAgEBPwH6USvyC1vubLSEzCgQfY8XMImWVA7fs5fTRFjmuzVrqg2vC/pCR2TM3+VvSK9bWfjlVjr9qu4NC2YKjJ7CLW7bdrcCi1GNxb8wjfnQ1saddtV3iALAOqID497FyEOoLDPJFHrtoOucpoyMjITdO7inXMVhByX1pNCypsO6TEGvunvY6EZfRQOtqkeW0QhNz5SE2VjtD2PdchKiOVoHWyVlNpkscmUMPEIwnM5n6Xf6hI9sVt1bqjPztbXAoOY6T5hCPi75qnNiPpSZqncHUmnlHWTmoysG25wqQhapMK0x5G+mikY5krZR9iowYpDF8dFvXyQOB8wUGGead6KT8wqPy9RxXHuCNmbNXHuljSbKeGRy5ydVhmZCQPKph4ih8qrp77SPwNpu5w0LZt2U+IONqNuQV7HNhGzGysNh9yK9pr90Eodlex19VP/EADUQAAEDAgMECQMDBQEAAAAAAAEAAhEDIRASMSJBUWETIDAyQHGBkbEEM0IjYnBQUlOiwfD/2gAIAQEABj8C/ijLMGLc10gfDZgzuWiuD/Q3AOylokFBz4LKtngfKNIVNqnek4/CH1MS3u1WrI+Id3HJ1i1zdWrM12zxWnj6z/IKkzg3AwSJQp7hpyTH5YeNeaOXuP1HBGiTb8CiD3h45zuL1Ud/ZA6znETdAxc+Opc7r6tv7uszndAcvGho3pmYEZDkdyKqZ29++D2vBzbrrpW1N8EHctpuDG8GqqfTxd8ARrKruqND6NQbWX5VGsfvUf8AZqyuEsrDNSdwKqVNKlIw9qEO2X6FUoN2b+CYC0PD9R/3Au4klHz8L3Vp1LQtCgdyfJtZQ2XeiHRsc0gyLo1n0cweMrwBqqv05BAac1ElUvqS2TTOWs1UHtnon3aZVRkw9o0KZ5J/g9FGHeC3+gX5exV40nCyusrhs7wCo6BqzUnmmV/kaFFRuUpwzAh2q6GiwvDTI/aqRcCK4s6UMwgo3w0WngdkepV5d5qTZQdFPwo0CEW8lkfv0PFXWs4XhfiVl6MHmpo1I5J/TGTmsqWfSVsIOaW4XHV17PVMZPewu2L6SrIQtmyheehUbzou8Vao4LZ+oPqv1qQcOIU0nD2R2x7L7hUNMjmE3KyzQrGPVarVa9TXtQ8fitYKmcJ381/75W2QPNEU25uBKzPMnqtqU/JzcLXXd9+xstO1mkcvJRBjkvyHou872RP6kdh0hGz8rRT1bq2FwtO316st2CrQ7yX23L7bvZWplbUN81J2jzwuVKjDjhceE1x1xuVxOBtfCOrb38XrhOGn8p//xAAsEAEAAgEDAwMDBAIDAAAAAAABABEhMUFREGFxIIGRQKGxMMHh8FDRYHDx/9oACAEBAAE/If8Aqhy9YHLiKqkUTl95sFxZTqdoLYjuSv8AAkRRqaXTcCjG75BJLNKsgwvYkeZWRHZlX2ZZOxTPxFtI6bYiD+yJX1pMUizkD8scMMBmDTrFkNveGfdGxo070zOnP7oJcVkWKsK/zH6wnPTfglcf+O/3mHTcuXA92qWSkcqvrSe7n3QfZn4/aLSkrqT3X+6KzaFo6/U2dpftBNoHGVREMnkvNMbVHXe8SkLL0mOCLeAdmNIdjZcDC5LKzfS7Zx9p2OQexKFK+kG7TZJskdoVKkp+0esBpU0SkUzUfq6rwYGCrU6OrsfFE67CG3PiPix3rX2lIY+jX+EKYg2Dw/1BUKW/nJ7SpTtGVcRZKf1QuPFlYi41eJcVeu8HiEYqHTEpMF2uUc0cmYWx1WxoqdF7XMgc1VjOlE6HmOQ4EHn5nBmRbxCD4wfeFNGb2L4jT3D+CXfAgBdGoT8qdpL52Iror9FY7HSMwNY7VH2mcclSmvUXkZhuV6Zv2lmncl0ILuPiNSi4JoVnFRa4TdJA6od8s7A0XZLtsNdUcvb96RGBFZazfql5jRpGFXNKeOcIaJQcKw1kbkRbxAO0TAjCeoMy9gS9guSFqqp/t3lAoL30f6lHQAyNUS1NBm7hqHvFQyTWv7+JgGe7XB8zAhfhxjcERoXhLtq8yix75Yhp9iF+8VqFb72UPdIZXiYfZVvxKQYnO0oYq5qDdn3gYI7S0FC+rMnNI+6JK9YAnCEJUuWYWNGTxcoEK/0Ok3cxOJe1RxrdQQmVti4jok3TNQCLpfaH95iS+1/W85/nl+8WwPPOHf5iHjlTNj/pqJnhD8IDeUbscSkW7UgzOIvMA2W0KOU+8t1hiam+nVUw2TDeO7oJE9NaRdiISnXnxtDN8jM/GK1hTSjPNffeDWd2G/b+Y6IZvn8weyhi21aQSxaYAf7j1KdSd7OGNjRZ2TNbfwS9oGEdQa0i1FfiAa1UdNEt1JZKLvKmCrlhtjWQWxTXoDkjH1Kqp4oFCwZmcfCCIXu3LJdxpcl7QrrUN3u4Qirl19IgxA1QcwCuG0sOw2lvXG0ZqDzKuFR3uN6dIUMMRVwl6C/iZWReWJTRODoP1hTuzudHNMeY3ZolzEvXxGvO/MUsV4yM02Ea/wDF0U9XuyMZn3Si/CiFaBVcQGdTUmMX2zEwnvG2Ab4gI4syperHWs94vA9UI2RYovQdQ7s70ODmUmYO2JeWU+XaeUxmsEapLTQYjKY4jIIhaN66S0aazV141goWDXG853lmJyYvWvQ7nRfqDD0D61ynoEhzmFdoHiA3GLaFHEtxaWxilpiWzAfKOWBiOgvZnea3uFAFY4mvXoW+lFZZZX9Ip1DuYTb3gIYTSQe6WW7mRPKBWDpmGUtqV6l7o98YZehZf1ly/Qb9e0OtEMMOjynn1WFf4AuXL/41/9oADAMBAAIAAwAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASCSSQAAAAAAAAAAAAAAbE4KSAAAAAAAAAAAAAAKwXmFAAAAAAAAAAAAAAICIoVgAAAAAAAAAAAAAKrySdwAAAAAAAAAAAAAIjiBDAAAAAAAQAAAAQSSOcXYIAAAAAAQAATJM01IbFjiAQQAAAAAAIYCSntEOZaQSQQAAASBAwCDvFBMISSQSSAAQQKZ0CGoAIIgkCCSCCAAARJDRRwBBN0SISQCQAARJIACuSQDJqZc3/AJAgAEkwgCkkggGWJAkAkQwAgkAkkgkCGA0yEEJsUQAgAAkggGODs0obpNAmAAEEAkEgzykBfoltOgAAAAEAgEtgEAlNJoekgAAAAAAkgMAElpgkkkgAAAAAAAAAAEkkkEkgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//8QAIhEAAwABBAICAwAAAAAAAAAAAAERIRAgMUBBUDBhYHBx/9oACAEDAQE/EP1QiFE9HRBhqh4TPOj6E9BfOrfAwmkxZfoBKx7bZESfeUJ2j2pCue6uRGQVpvsTIslSkMkEMq+zCMjJoNlSyMaT/oeaeBpoN0qaIwF5Go+xS6xkx4Gyxcwo2Q5ExJEaHmRkOfUQ9cGNFogez6Qjgi/Oly6rRGRk0XImXV7IVjqJDcBr21vQ/uTCfBS7aUpSlL3aX02e6kRaUpSl1naYugvRrrTfCE3T89//xAApEQADAAECBQMEAwEAAAAAAAAAAREhEDEgQEFRYTBxwVCBoeFgsdFw/9oACAECAQE/EP8AgU+io2d+hbvyXleCjmTcYRPSfQEKKqm5DGb82+BIfDBfkc7uPdvge6hYT5NsibGJc2+ljnUSU8i+5P4FsIZNrYk6rLEizD28CTfA/A0JVO6Nrcc77FO1X+in3c/yJ1aXSsQ2zA7Ms55z3bB41fsdOiaomO0IN3c6t20iwbcPK8FwXBfgRRGPEbyMubFT7/vwKaC2pTu477kJ45WlRRl1orZH/Q5nPxfyjC2C+w21Nid13EN2k1PyNyTfUX+FmLK/L9mDOurr7MyTqylLskizOUvoObW8Hov3H0dBe43UfkTWyrWYxeFuFZ+5Y8WT+ew8t6Jos+5mAZKY5WEZOwgejSYggKXR5FpGVsJ87pNdGU7WyvAhOr2Gq8oQiG2hMaT0FpngTmS/RF/ZZnRFe3H5F5Bl0gkPS6VaPgmthU0Zbqi1Wj9dMpeG8cIY6FKJaEiEXBCehCCXDfSpuQmBFLpGPbhaJyd0ui0miROB8NG+R2I6oIiISRCFKUuj4XyKGMLhWj9G/Xr6NL/O/wD/xAArEAEAAgICAQIFBAIDAAAAAAABABEhMUFRYXGBIJGhsdEQQOHwMMFQYPH/2gAIAQEAAT8Q/wCvV/weJuVK/wCDCZ7uMZLg8XPlNSi669ZirulL3Dkhd+aFQX1lzuIzMz1Pb98CIq7BFYvW42oETSAwOHMuTYh3LXBUsmz4TiAOfPhpDcyyvMyO7TMAsCw9JmpXlPTkPpGaJk/e8pmRijx6v4nO1j/TxLKdQsIU4B5OYP8AvdyPG0xroWx5jiChEqJVnPmDos9w1mMIwypk9xX6/vVyGkewD8xsip6A2fZBcS2WloN2teszJ5NFBrrcwdWiOwPzH96xLCX93+ItWwvToPwS1gSzM0ifoLj38IV9V/onMZP3f4mC/cBcNBMOZnOZZZOXatQVMCCwNX1F58xgvjUMGw44mRAthq+5pNF2noNX3DMgt/1CYp7gJmQYB2I5PMBaqcRKXrt9YdJd9+Z944KcKSxvEr9lStR7CmetU3Lrth+DhaTDpXgeIZLr2gTH2p1KkLCrTe412NLDNDkOagORus2f7CrPWLsg86bb9O6Yr7sTat7lH2YNYc2hL9R4dQEJlKo+L34bleqlATjKV7+qFbjrqBQt9pVX0CJHiwSpf2Rzj6Tw02hG7Up/wHwKsDOPZlle5AYrsQxU1TdbPlAZQPFGPlKUMW9uvEAwLrNZhXuORUK5zG0wg0ZLxRx5iVCSUaN83qGcuroRF84uMA/lUT3h4YksqdDuU0KgJqhGoVtjFJN3eHj3YQDbaKWGRk9fPiKNSUAq5XrKfOOUuQVamPLZMgHBN96ZghsC4lpRp+coLXquDvPhB0L9DEUuoJmPqijj4j9AvU2Ax2tpWBFHPcGkHam4QcXXL0mGWioRQ9CJq3ErU5tDFe1SvQVsszfHSJqA6FyXeCndxxoVaf08RBhJu6/1LAEMu2f6wEFUQU7rFelRPoJWp5tgW4KtQ/3E9mMmOu+YX5HcndMFFVCoXh9oOsZApbseTeNw6LJbSabdy+1V2W8ZZdaWQ8w3U9RRzetwAZBzUYCrW6gnC5nFOupxj9OllfARGACyM4zDIQqCUAN51+I6GRGcGmkD7sEG5gvHL4NMnN682C3yQMitmKVxpM1cWR3JVxvjGDC5t241NoEQGKbo0Xbxi6xrLLAuavrOjOPSg8wOA6dbVuwFU5cjdxZvdswBlHp+/ECqIVw9QNczrLccrYJiuT65iLoAgIa9GL/aD69sqCTQD5b3j0iclVySr7mAfFakGX6/SbuTQ2FAvyv0ljizIMCOR41bpiLQ6pBNZzpiHGeFgXn3ijBAPYPBCS78JV/MdtxK+CxJlGpUOsykNM1cYFTcqWBdX5hABKQRau8F16X6VLrsu0w4r4dcQbBUgpDRzRh4y/xDBIWyOjuiq1lICVslqJd0Jmt4qszTxWweUssqrxXyiQBMIdnaF9jgi/aNA1TVWO8MXxjvA7fgNvzCSDtD7Mr3bjb5GER5sVD8x+kAPlBS8ol4YJRLPOjxfmJXZAb5T+5jHXAQcbsyOiL3WCUtcpz1EiGiiISO1S4QtifOD3YGlGyBRNnyg7wXqrZYuw3T4lDir0gW2D2imFX4lF39oZ38ItekyAlL9xgBaVCF+NoufbEKxo1oNHTtu55CAA1T00SihEpVjTaWdsfeYrCFORHh9/C4gBVmri9D7Y1iLYeoN4GhxznHiAgHAVjBgpoaHHicvZlwHAHB+thxh8TAhfsLGKivcGtFh1plXhWy8mPeiJFVU6/iFgdhpD2ps8zCohnjXB6RQhQp1liumbQdvu9xsoTAibIJq23a7mUW9033G4OixJgWZ1jHvMCF1UGInbBwc15jmMc0QhUVcvL5+BVUprF1CAsCALsVcLsJp1X+4+W/IW+8JqNi1Xhtax7yhIgBorxUEjNeAr5auXcy2gF9sEUUVcsv4LswKKcTgO2n0HUDZBooibtF6HHUTdHJtW6g8KYIgx+JYUSi6lbzj1loGg7y+v2iPQDNON9SnYKUja37TVpaSA2TUdXtAi1TmiFa4MLqGfOMW6hFgq6+CuKYtKayZS1bneZqCBuO5HeXEQybfLOkDlBv3iAUti/DzAWxWCDD4idyYDY8n4jTkWLUvzlP78ufSfbN4QRvwfeFO/CLq+PzHwJ5x+gQMNYPAr+kOLWAZrzXtH661VskOMSkRTVN60NQjUEpGM8P9zLaVLZa/wBIL4KVS0B4i7BZ7TeyekDuVNk3Cwm6YPcPlN0olnwJlCZlVZrmeNmVIoohJSK2Esdm6qVJWhWyIKNOaTnuIKB6oM8Jp3BIX/2UYIvCOoCJC0a8wkG9DdntKKTKS0U1Y+8oqHJqjN0kQJLlS2tfxBN2Iqlf+xeBs8484jQ29y4ybpS2GagopRlFN+CULacVwARBKuflEZGX3mUYueaeaUy5jn4RqFS5YMStHUrL7lKjBxN3YO4BpAXsYAbzmqYobXIcsD5F7byEYg5bu7xXD6wqJABbmM2F0NV6R1NEao36wXgGk/iFKqq4cf3E0C0Bl5uULar3VzK5ooVB/uZgQpWNVOy4QS6r5zgMNX8wa3B7mVl02TNFf8I5/RD4YK7vfUbs95Y4a9YYE9UsXp8kcpbVU1LYKw2wZyroaiBZdWi4uIppVz6Rd5WauKBbKrBrzMSil2rTmUgAUZB4j5Hzgrv9FSwXc80v5lhueSIy7/ykFXcH3Lu2Ww8zzS/cvWHMyWsQuFTUtxcIIuI+FxfctCnUKTbF9stNxHMv3FMv9pf6XFWWy2Wy2XFMsS0tlv8A1j//2Q=="/>
    <footer style="
position: fixed;
width: 100%;
font-size: small;
bottom: 6mm;
text-align: center;
color: white;
display: none">
        Developed by <a style="text-decoration: none; font-weight: bold; color: white"
                        href="http://kissarat.github.io/">zenothing.com</a>
    </footer>
</div>
<noscript>Site Under Maintenance. JavaScript is disabled</noscript>

<script>
    var lang = navigator.language;
    if (lang) {
        var t = {
            en: 'Site Under Maintenance',
            ru: 'Ведутся технические работы',
            uk: 'Ведуться технічні работи'
        };

        for (var l in t) {
            if (lang.indexOf(l) >= 0) {
                document.title = document.getElementById('text').innerHTML = t[l];
                document.head.setAttribute('lang', l);
            }
        }
    }
    document.body.style.backgroundColor = 'black';
    root.style.removeProperty('display');
</script>
</body>
</html>
