<?php
$files = scandir('dieKah3kahv1shi0moez', SCANDIR_SORT_DESCENDING);
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="robots" content="none"/>
    <meta name="contact" content="kissarat@gmail.com"/>
    <meta name="author" content="Taras Labiak"/>
    <title>Statistics</title>
    <style>
        a {
            display: block;
        }

        pre {
            background-color: lightcyan;
        }
    </style>
</head>
<body>
<article>
    <?php
    foreach ($files as $file) {
        if ('.' != $file[0]) {
            $time = str_replace('.json', '', $file);
            echo "<a href=\"/dieKah3kahv1shi0moez/$file\">$time</a>";
        }
    }
    ?>
</article>
<script>
    Array.from(document.querySelectorAll('a')).forEach(function (anchor) {
        var time = parseInt(anchor.innerHTML);
        anchor.innerHTML = new Date(Math.round(time / 10)).toLocaleString();
        anchor.onclick = function (e) {
            var next = anchor.nextElementSibling;
            if (next && 'PRE' == next.tagName) {
                next.remove();
            }
            else {
                var xhr = new XMLHttpRequest();
                xhr.open('GET', '/dieKah3kahv1shi0moez/' + time + '.json');
                xhr.onload = function () {
                    var data = JSON.parse(xhr.responseText);
                    if (data.server) {
                        data.download = data.start - (data.server / 10);
                    }
                    var pre = document.createElement('pre');
                    pre.innerHTML = JSON.stringify(data, null, '\t');
                    anchor.parentNode.insertBefore(pre, next);
                };
                xhr.send(null);
            }
            e.preventDefault();
        }
    });
</script>
</body>
</html>
